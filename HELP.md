# Setup help

## Linux

### Required packages
[readme-linux]: https://github.com/WootingKb/wooting-rgb-sdk/blob/master/README.md#linux

The linux version of the Wooting RGB SDK depends on a few software packages in
 order to function. Please consult [the SDK's README][readme-linux] for
 up-to-date information.

At the time of writing, these packages were declared as being required:
- `libusb-1.0`
- `libusb-dev`
- `hidapi`
- `libhidapi-dev`

If you don't know how to install them, please consult your system administrator.


### Keyboard is not being detected
Here's [a help article](https://wooting.helpscoutdocs.com/article/68-wootility-configuring-device-access-for-wootility-under-linux-udev-rules).
