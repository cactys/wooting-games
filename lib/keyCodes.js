const os = require('os')

/**
 * @todo Find a library that does this. This is awful.
 */

let keys = {}
switch(os.type()) {
	case 'Windows_NT': {
		keys = {
			enter: 13,
			escape: 27,
			space: 32,
			left: 37,
			up: 38,
			right: 39,
			down: 40,
			r: 82,
			f1: 112,
			f12: 123,
			numLock: 144,
			ctrlR: 163
		}
		break
	}
	case 'Linux': {
		keys = {
			enter: 65293,
			escape: 65307,
			space: 32,
			left: 65361,
			up: 65362,
			right: 65363,
			down: 65364,
			r: 114,
			f1: 65470,
			f12: 65481,
			numLock: 65407,
			ctrlR: 65508
		}
		break
	}
}
module.exports = keys
