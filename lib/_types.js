/**
 * A color in RGB format
 * @typedef {Array<{red: number, green: number, blue: number}>} RgbColor
 */

/**
 * A two-dimensional position
 * @typedef {Object} Position2d
 * @property {number} x - An `x` position in the game's coordinate system
 * @property {number} y - An `y` position in the game's coordinate system
 */
