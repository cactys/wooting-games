/**
 * This is a game inspired by Tetris that can be played on your Wooting keyboard.
 * Due to limited space, the bricks only consist of 3 components. These are so-called "Trominoes".
 *
 * There is optional extra functionality for Wooting 2 keyboards: The numblock displays a preview for the next brick.
 */
/**/

// npm packages

const ioHook = require('iohook')


// utils

const wooting = require('../wooting')
    , keyCodes = require('../keyCodes')
    , utils = require('../utils')


/**
 * Constants
 */
const constants = {
	maxLevel: 12,
	levelupModifier: .2,
	baseSpeed: 1000,
	worldSize: { width: 4, height: 10 },
	colors: {
		white: [255, 255, 255],
		gray: [7, 7, 7],
		red: [255, 0, 0],
		green: [0, 255, 0],

		void: [7, 0, 0],
		clear: [0, 0, 0],
		get death() { return this.red },

		trominoes: utils.generateNiceColors()
	}
}

/**
 * An enum of available moves
 * @type {{DOWN: string, LEFT: string, ROTATE: string, RIGHT: string, UP: string}}
 */
const Moves = {
	UP: '',
	DOWN: '',
	LEFT: '',
	RIGHT: '',
	ROTATE: ''
}
Object.keys(Moves).forEach(key => Moves[key] = key)

/**
 * The game class
 * @extends AbstractGame
 */
module.exports = class TrominoGame extends require('../AbstractGame') {
	/**
	 * Translates given position to the game's coordinate system
	 * @param {Position2d} pos The position to translate
	 * @return {Position2d}
	 */
	static translateGamePos(pos) {
		const { x, y } = pos
		return {
			x: 4 - x,
			y : y + 1 + Number(x === 0)
		}
	}

	/**
	 * The constructor of the game
	 * @param {WootingRgbSdkWrapper} api
	 */
	constructor(api) {
		super()

		this.paused = false
		this.tickTimeout = 0
		this.level = 1
		this.showPreview = true
	}

	/**
	 * Game state management functions:
	 */

	/**
	 * Initiates the game, clears colors and highlights control buttons
	 * @return {Promise<void>}
	 */
	async start() {
		// Clear the main part of the keyboard
		for(let row = 0; row < wooting.keyboardDimensions.rows; row++) {
			for(let col = 0; col < wooting.keyboardDimensions.columns; col++) {
				if(wooting.isWootingTwo) {
					if(17 <= col && col <= 19 && 2 <= row && row <= 4) continue
				}
				wooting.setArrayKey(row, col, constants.colors.void)
			}
		}

		// highlight controlsIndicators
		wooting.setArrayKey(0, 0, constants.colors.red) // ESC

		wooting.setArrayKey(4, 15, constants.colors.white) // Arrow up
		wooting.setArrayKey(5, 14, constants.colors.white) // Arrow left
		wooting.setArrayKey(5, 15, constants.colors.white) // Arrow down
		wooting.setArrayKey(5, 16, constants.colors.white) // Arrow right

		wooting.setArrayKey(5, 6, constants.colors.white) // Space
		wooting.setArrayKey(5, 13, constants.colors.white) // CTRL right


		this.reset()
		this.startIoHook()

		return super.start()
	}

	/**
	 * Resets the game
	 */
	reset() {
		this.clearScene()

		this.matrix = Array(constants.worldSize.height).fill(undefined)
			.map(() => Array(constants.worldSize.width).fill(false))

		this.togglePreview(this.showPreview)

		this.newBrick(true)
		this.setLevel(1)

		wooting.flushKeys()
	}

	/**
	 * Ends the game
	 * @param {boolean} death Specifies whether the player died
	 */
	end(death) {
		ioHook.stop()
		ioHook.removeAllListeners()
		clearTimeout(this.tickTimeout)

		// Drawing the death color from left to right, inspired by Tetris (hence not just painting a whole array)
		for(let col = 0; col < wooting.keyboardDimensions.columns; col++) {
			for(let row = 0; row < wooting.keyboardDimensions.rows; row++) {
				if(row === 0 && col === (this.state.level + 1)) continue // keep level indicator
				wooting.setArrayKey(row, col, death ? constants.colors.death : constants.colors.gray)
			}
			wooting.flushKeys()
		}

		setTimeout(() => {
			// Resetting the keys one by one, gives a nice " wrapping things up" effect
			for(let col = 0; col < wooting.keyboardDimensions.columns; col++) {
				for(let row = 0; row < wooting.keyboardDimensions.rows; row++) {
					wooting.setArrayKey(row, col, constants.colors.clear)
				}
				wooting.flushKeys()
			}

			this.fork.resolve()
		}, death ? 3000 : 0)
	}

	/**
	 * Performs a game logic step
	 */
	tick() {
		this.move(Moves.DOWN)
	}

	/**
	 * Adjusts the tick rate for the current level and sets off a timeout for the logic tick
	 */
	updateTickRate() {
		clearTimeout(this.tickTimeout)

		const delay = ((constants.maxLevel + 1) - this.level) * (constants.baseSpeed / constants.maxLevel)
		this.tickTimeout = setTimeout(() => this.tick(), delay)
	}

	/**
	 * Sets the currentlevel
	 * @param {number} level The level to set the game to, maxed to 12
	 */
	setLevel(level) {
		if(this.level) {
			wooting.setKey(0, 1 + Math.floor(this.level), constants.colors.void)
		}

		this.level = Math.max(0, Math.min(level, constants.maxLevel, 12))
		wooting.setKey(0, 1 + Math.floor(this.level), this.level < constants.maxLevel ? constants.colors.white : constants.colors.red)

		this.updateTickRate()
	}

	/**
	 * Rendering functions:
	 */

	/**
	 * Draws to a key in the game's coordinate system
	 * @param {Position2d} pos The position of the key
	 * @param {RgbColor} [color] The color for the key
	 */
	drawKey(pos, color) {
		color = color || constants.colors.clear

		const { x, y } = TrominoGame.translateGamePos(pos)
		wooting.setArrayKey(x, y, color.map(value => value * (!this.paused ? 1 : .1)))
	}

	/**
	 * Clears the game area
	 */
	clearScene() {
		for(let y = 0; y < constants.worldSize.height; y++) {
			for(let x = 0; x < constants.worldSize.width; x++) {
				this.drawKey({ x, y }, constants.colors.clear)
			}
		}
	}

	/**
	 * Renders the blocks sitting in the game area
	 */
	renderScene() {
		this.matrix.forEach((row, y) => {
			row.forEach((col, x) => {
				this.drawKey({ x, y }, this.matrix[y][x])
			})
		})
	}

	/**
	 * Renders or clears the controllable brick and / or the preview brick
	 * @param {{brick: boolean = false, preview: boolean = false, clear: boolean = false}} [options] The options for this rendering call
	 */
	renderBrick(options = {}) {
		options = {
			brick: !!options.brick || false,
			preview: !!options.preview || false,
			clear: !!options.clear || false
		}

		if(options.brick) {
			const color = options.clear ? constants.colors.clear : this.state.color
			this.state.shape
				.map(component => this.getComponentPos(component))
				.forEach(pos => {
					this.drawKey(pos, color)
				})
		}

		if(options.preview && wooting.isWootingTwo) {
			const color = options.clear ? constants.colors.clear : this.state.next.color
			this.state.next.shape
				.map(component => this.getComponentPos(component, { x: 18, y: 3 }, (this.state.next.rotation + 3) % 4))
				.forEach(pos => {
					wooting.setArrayKey(pos.y, pos.x, color)
				})
		}
	}

	/**
	 * Game mechanism functions:
	 */

	/**
	 * Adds a new conrollable brick to the game
	 * @return {boolean} Whether or not the game will continue
	 */
	newBrick(reset) {
		if(this.state) {
			this.renderBrick({
				brick: false,
				preview: this.showPreview,
				clear: true
			})
		}

		if(reset) this.state = { next: undefined }

		const newBrick = this.generateBrick()

		this.state = this.state || { next: undefined }
		this.state = {
			position: (this.state.next || newBrick).position,
			rotation: (this.state.next || newBrick).rotation,
			shape: (this.state.next || newBrick).shape,
			color: (this.state.next || newBrick).color,
			next: this.state.next ? newBrick : this.generateBrick()
		}

		if(this.collides()) return false

		this.renderBrick({
			brick: true,
			preview: this.showPreview
		})
		return true
	}

	/**
	 * Generates a new brick for future use
	 */
	generateBrick() {
		const newBrick = {
			position: { x: 1, y: 1 },
			rotation: Math.floor(Math.random() * 4),
			shape: Math.random() < .5 ? [
				{ offset: { x: -1, y: 0} },
				{ offset: { x: 0, y: 0} },
				{ offset: { x: 0, y: 1} }
			] : [
				{ offset: { x: -1, y: 0} },
				{ offset: { x: 0, y: 0} },
				{ offset: { x: 1, y: 0} }
			],
			color: constants.colors.trominoes[Math.floor(Math.random() * constants.colors.trominoes.length)]
		}

		const componentPositions = newBrick.shape.map(component => this.getComponentPos(component, newBrick.position, newBrick.rotation))

		if(componentPositions.some(pos => pos.x === 0) && !componentPositions.some(pos => pos.x === 2)) newBrick.position.x++
		if(!componentPositions.some(pos => pos.y === 0)) newBrick.position.y--

		return newBrick
	}

	/**
	 * Performs a move for the controllable brick
	 * @param {Moves} move
	 */
	move(move) {
		let newPos = Object.assign({}, this.state.position)
		let newRot = this.state.rotation

		switch(move) {
			case Moves.UP:      newPos.y--; break
			case Moves.DOWN:    newPos.y++; break
			case Moves.LEFT:    newPos.x--; break
			case Moves.RIGHT:   newPos.x++; break
			case Moves.ROTATE:  newRot = (newRot + 1) % 4; break
		}

		if(!this.collides.at(newPos, newRot)) {
			this.renderBrick({
				brick: true,
				preview: this.showPreview,
				clear: true
			})

			this.state.position = newPos
			this.state.rotation = newRot

			this.renderBrick({
				brick: true,
				preview: this.showPreview
			})
		} else if(move === Moves.DOWN) {
			this.state.shape
				.map(component => this.getComponentPos(component))
				.forEach(pos => {
					this.matrix[pos.y][pos.x] = this.state.color
				})

			let clearedLine = false
			this.matrix.forEach((row, index) => {
				if(row.every(v => v)) {
					clearedLine = true
					this.clearLine(index)
				}
			})

			if(clearedLine) this.setLevel(this.level + constants.levelupModifier)

			if(!this.newBrick()) return this.end(true)
		}

		if(move === Moves.DOWN) this.updateTickRate()
		wooting.flushKeys()
	}

	/**
	 * Returns the position of a brick's `component` relative to the given `pos`ition and influenced by the given `rot`ation
	 * @param {{offset: Position2d}} component
	 * @param {Position2d} [pos] The relative position to use for calculation of the position
	 * @param {number} [rot] The rotation to use for calculation of the position
	 * @return {Position2d} The position of the component in the game's coordinate system
	 */
	getComponentPos(component, pos, rot) {
		pos = pos || this.state.position
		rot = rot !== undefined ? rot : this.state.rotation // input could be 0

		switch(rot) {
			case 0: {
				return {
					x: pos.x + component.offset.x,
					y: pos.y + component.offset.y,
				}
			}
			case 1: {
				return {
					x: pos.x - component.offset.y,
					y: pos.y + component.offset.x,
				}
			}
			case 2: {
				return {
					x: pos.x - component.offset.x,
					y: pos.y - component.offset.y,
				}
			}
			case 3: {
				return {
					x: pos.x + component.offset.y,
					y: pos.y - component.offset.x,
				}
			}
			default: return {
				x: pos.x,
				y: pos.y
			}
		}
	}

	/**
	 * Clears a line and inserts a new one at the top respectively
	 * @param {number} index The index of the line to clear
	 */
	clearLine(index) {
		this.matrix.splice(index, 1)
		this.matrix.splice(0, 0, Array(constants.worldSize.width).fill(undefined).map(() => false))
		this.renderScene()
	}

	/**
	 * Toggles the preview moved. Only supported on Wooting 2
	 * @param {boolean} [toggle] The state to toggle preview mode to
	 */
	togglePreview(toggle) {
		if(!wooting.isWootingTwo) return

		toggle = toggle !== undefined ? toggle : !this.showPreview
		this.showPreview = toggle

		wooting.setArrayKey(1, 17, this.showPreview ? constants.colors.green : constants.colors.red)

		if(!this.state) return
		this.renderBrick({
			brick: false,
			preview: true,
			clear: !this.showPreview
		})

		wooting.flushKeys()
	}

	/**
	 * @return {function(): boolean} Whether the currently controllable brick would collide with anything (walls, other bricks) or not
	 */
	get collides() {
		/**
		 * @param {Position2d} pos The position of a brick to imply for this calculation
		 * @param {number} rot The rotation of a brick to imply for this calculation
		 * @return {boolean} Whether a brick at given `pos`ition with given `rot`ation would collide with anything (walls, other bricks) or not
		 */
		const collidesAt = (pos, rot) => {
			pos = pos || this.state.position
			rot = rot !== undefined ? rot : this.state.rotation

			return this.state.shape
				.map(component => this.getComponentPos(component, pos, rot))
				.some(pos => {
					if(pos.x < 0 || pos.x >= this.matrix[0].length) return true
					if(pos.y < 0 || pos.y >= this.matrix.length) return true
					return this.matrix[pos.y][pos.x]
				})
		}

		const func = () => { return collidesAt(this.state.position, this.state.rotation) }
		func.at = collidesAt

		return func
	}

	/**
	 * Starts polling key events with the `iohook` npm package
	 */
	startIoHook() {
		ioHook.start(false)
		ioHook.on('keydown', event => {
			if(event.rawcode === keyCodes.enter) {
				this.paused = !this.paused

				if(this.paused) {
					clearTimeout(this.tickTimeout)
				} else {
					this.updateTickRate()
				}

 				this.renderScene()
				this.renderBrick({
					brick: true,
					preview: this.showPreview
				})
				wooting.flushKeys()

				return
			} else if(this.paused) return

			if(keyCodes.f1 <= event.rawcode && event.rawcode <= keyCodes.f12) { // F1 - F12 keys
				this.setLevel(event.rawcode - (keyCodes.f1 - 1))
			}

			switch(event.rawcode) {
				case keyCodes.escape: {
					this.end(false)
					break
				}
				case keyCodes.r: {
					this.reset()
					break
				}
				case keyCodes.numLock: {
					this.togglePreview()
					break
				}

				case keyCodes.left: {
					let initialState = this.state
					do {
						this.move(Moves.DOWN)
					} while(this.state === initialState)
					break
				}
				case keyCodes.right: {
					this.move(Moves.DOWN)
					break
				}
				case keyCodes.down: {
					this.move(Moves.LEFT)
					break
				}
				case keyCodes.up: {
					this.move(Moves.RIGHT)
					break
				}
				case keyCodes.space:
				case keyCodes.ctrlR: {
					this.move(Moves.ROTATE)
					break
				}
			}
		})
	}
}
