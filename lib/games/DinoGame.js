/**
 * This is a game inspired by Google Chrome's dino game that appears in tabs when you are offline.
 * You jump over cacti and try to survive.
 */
/**/

// npm packages

const ioHook = require('iohook')


// utils

const wooting = require('../wooting')
    , keyCodes = require('../keyCodes')


/**
 * Constants
 */
const constants = {
	maxLevel: 6,
	levelupModifier: .25,
	baseSpeed: 400,
	worldSize: { width: 10, height: 4 },
	cactusRate: {
		spawn: .25,
		double: .9,
		small: 1 / 3
	},
	colors: {
		white: [255, 255, 255],
		gray: [7, 7, 7],
		red: [255, 0, 0],
		green: [0, 255, 0],

		void: [7, 0, 0],
		clear: [0, 0, 0],
		get death() { return this.red },
		get cactus() { return this.green },
		dino: [
			[255, 0, 0],
			[0, 0, 255],
			[255, 255, 0],
			[0, 255, 255],
			[255, 0, 255]
		]
	}
}

/**
 * The game class
 * @extends AbstractGame
 */
module.exports = class DinoGame extends require('../AbstractGame') {
	/**
	 * Translates given position to the game's coordinate system
	 * @param {Position2d} pos The position to translate
	 * @return {Position2d}
	 */
	static translateGamePos(pos) {
		const { x, y } = pos
		return {
			x: x + 1 + Number(y === 0),
			y : 4 - y
		}
	}

	/**
	 * The constructor of the game
	 */
	constructor() {
		super()
		this.paused = false
		this.tickInterval = 0
	}

	/**
	 * Game state management functions:
	 */

	/**
	 * Initiates the game, clears colors and highlights control buttons
	 * @return {Promise<void>}
	 */
	async start() {
		/* Clear the main part of the keyboard */
		for(let row = 0; row < wooting.keyboardDimensions.rows; row++) {
			for(let col = 0; col < wooting.keyboardDimensions.columns; col++) {
				wooting.setArrayKey(row, col, constants.colors.void)
			}
		}

		// highlight controlsIndicators
		wooting.setArrayKey(0, 0, constants.colors.red) // ESC
		wooting.setArrayKey(4, 15, constants.colors.white) // Arrow up
		wooting.setArrayKey(5, 6, constants.colors.white) // Space


		this.reset()
		this.startIoHook()

		return super.start()
	}

	/**
	 * Resets the game
	 */
	reset() {
		this.clearScene()
		this.newGame()
	}

	/**
	 * Ends the game
	 * @param {boolean} death Specifies whether the player died
	 */
	end(death) {
		ioHook.stop()
		ioHook.removeAllListeners()
		clearInterval(this.tickInterval)

		for(let row = 0; row < wooting.keyboardDimensions.rows; row++) {
			for(let col = 0; col < wooting.keyboardDimensions.columns; col++) {
				if (row === 0 && col === (this.state.level + 1)) continue // keep level indicator
				wooting.setArrayKey(row, col, death ? constants.colors.death : constants.colors.gray)
			}
			wooting.flushKeys()
		}

		setTimeout(() => {
			for(let row = 0; row < wooting.keyboardDimensions.rows; row++) {
				for(let col = 0; col < wooting.keyboardDimensions.columns; col++) {
					wooting.setArrayKey(row, col, constants.colors.clear)
				}
				wooting.flushKeys()
			}

			this.fork.resolve()
		}, death ? 3000 : 0)
	}

	/**
	 * Performs a game logic step (scrolls the scene)
	 */
	tick() {
		this.shiftWorld()

		this.state.framesSinceJump++

		const wouldDie = !!this.state.cacti[this.state.position.x]
		const willDie = wouldDie && this.state.position.z < this.state.cacti[this.state.position.x]

		if(willDie) return this.end(true)
		if(wouldDie) {
			this.setLevel(this.state.level + constants.levelupModifier)
		}

		this.render()
	}

	/**
	 * Adjusts the tick rate for the current level and sets off an tickInterval for the logic tick
	 */
	updateTickRate() {
		clearInterval(this.tickInterval)

		const delay = ((constants.maxLevel + 1) - this.state.level) * (constants.baseSpeed / constants.maxLevel)
		this.tickInterval = setInterval(() => this.tick(), delay)
	}

	/**
	 * Sets the current level
	 * @param {number} level The level to set the game to, maxed to 12
	 */
	setLevel(level) {
		if(this.state.level) {
			wooting.setKey(0, 1 + Math.floor(this.state.level),  constants.colors.void)
		}

		this.state.level = Math.max(0, Math.min(level, constants.maxLevel))
		wooting.setKey(0, 1 + Math.floor(this.state.level), this.state.level < constants.maxLevel ? constants.colors.white : constants.colors.red)

		this.updateTickRate()
	}

	/**
	 * Rendering functions:
	 */

	/**
	 * Draws to a key in the game's coordinate system
	 * @param {Position2d} pos The position of the key
	 * @param {RgbColor} [color] The color for the key
	 */
	drawKey(pos, color) {
		color = color || constants.colors.clear
		const { x, y } = DinoGame.translateGamePos(pos)

		wooting.setArrayKey(y, x, color.map(value => value * (!this.paused ? 1 : .25)))
	}

	/**
	 * Clears the game area
	 */
	clearScene() {
		for(let y = 0; y < constants.worldSize.height; y++) {
			for(let x = 0; x < constants.worldSize.width; x++) {
				this.drawKey({ x, y }, constants.colors.clear)
			}
		}
	}

	/**
	 * Renders the scene and the dino
	 */
	render() {
		const dinoPos = this.state.position

		this.state.cacti.forEach((cactusHeight, x) => {
			Array(constants.worldSize.height).fill(0)
				.forEach((_, z) => {
					this.drawKey({ x, y: z }, cactusHeight && z < cactusHeight ? constants.colors.cactus : constants.colors.clear)
				})
		})

		;[0, 1].forEach(zOffset => this.drawKey({ x: dinoPos.x, y: dinoPos.z + zOffset }, this.state.color))

		wooting.flushKeys()
	}

	/**
	 * Game mechanism functions:
	 */

	/**
	 * Sets up the state for a new game
	 */
	newGame() {
		this.state = {
			cacti: Array(constants.worldSize.width).fill(0),
			framesSinceJump: 6,
			framesSinceCactus: 0,

			get position() {
				let z = 0
				if(this.framesSinceJump === 1 || this.framesSinceJump === 5) {
					z = 1
				} else if(this.framesSinceJump < 5) {
					z = 2
				}

				return { x: 1, z }
			},
			color: (() => {

				return constants.colors.dino[Math.floor(Math.random() * constants.colors.dino.length)]
			})(),
			level: (this.state || {level: 1}).level
		}

		this.render()
		this.setLevel(1)
	}

	/**
	 * Shifts the world by one key and maybe spawns a new cactus
	 */
	shiftWorld() {
		this.state.cacti.splice(0, 1)

		let newCactus = Math.random() < constants.cactusRate.spawn || this.state.framesSinceCactus > 20

		if(newCactus && this.state.cacti.slice(-1)[0]) { // potential double cactus! Check for spawn permission
			if(Math.random() >= constants.cactusRate.double) {
				newCactus = false
			}
		}

		if(this.state.cacti.slice(-4).reduce((mem, cur) => mem + cur) >= 2) { // prevent too many cacti in one place
			newCactus = false
		}

		if(newCactus) this.state.framesSinceCactus = 0

		this.state.cacti.push(newCactus ? (1 + Number(Math.random() >= constants.cactusRate.small)) : 0)
	}

	/**
	 * Starts polling key events with the `iohook` npm package
	 */
	startIoHook() {
		ioHook.start(false)
		ioHook.on('keydown', event => {
			if(event.rawcode === keyCodes.enter) { // Return
				this.paused = !this.paused

				if(this.paused) {
					clearInterval(this.tickInterval)
				} else {
					this.updateTickRate()
				}

				this.render()

				return
			} else if(this.paused) return

			if(keyCodes.f1 <= event.rawcode && event.rawcode <= keyCodes.f12) { // F1 - F12 keys
				this.setLevel(event.rawcode - (keyCodes.f1 - 1))
			}

			switch(event.rawcode) {
				case keyCodes.escape: {
					this.end(false)
					break
				}
				case keyCodes.r: {
					this.reset()
					break
				}

				case keyCodes.up:
				case keyCodes.space: {
					if(this.state.framesSinceJump > 5) this.state.framesSinceJump = 0
					break
				}
			}
		})
	}
}
