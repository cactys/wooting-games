/**
 * This is a main menu that lets the player select between our two games.
 */
/**/

// npm packages

const ioHook = require('iohook')


// utils

const wooting = require('./wooting')
    , exitCodes = require('./exitCodes')
    , keyCodes = require('./keyCodes')
    , utils = require('./utils')


/**
 * Constants
 */
const constants = {
	colors: {
		clear: [0, 0, 0],
		gray: [7, 7, 7],
		red: [255, 0, 0],
		orange: [255, 127, 0],
		green: [0, 255, 0],
		purple: [127, 0, 127],
		white: [255, 255, 255]
	}
}

/**
 * The menu class
 */
module.exports = class Menu {
	/**
	 * "Opens" the menu by creating a new instance and starting the logic
	 * @return {Promise<void>} A promise that will indicate if a game was chosen or not
	 */
	static open() {
		return new Promise((resolve, reject) => {
			const menu = new this()
			menu.fork = { resolve, reject }
			menu.start()
		})
	}

	/**
	 * Clears the keyboard
	 */
	static clearKeyboard() {
		for(let row = 0; row < wooting.keyboardDimensions.rows; row++) {
			for(let col = 0; col < wooting.keyboardDimensions.columns; col++) {
				wooting.setArrayKey(row, col, constants.colors.clear)
			}
		}
		wooting.flushKeys()
	}

	/**
	 * Draws a big "Hi" to the center of the keyboard
	 */
	static drawGreeting() {
		// H
		wooting.setArrayKey(2, 3, constants.colors.white)
		wooting.setArrayKey(3, 3, constants.colors.white)
		wooting.setArrayKey(4, 4, constants.colors.white)

		wooting.setArrayKey(3, 4, constants.colors.white)

		wooting.setArrayKey(2, 5, constants.colors.white)
		wooting.setArrayKey(3, 5, constants.colors.white)
		wooting.setArrayKey(4, 6, constants.colors.white)

		// i
		wooting.setArrayKey(2, 7, constants.colors.purple)
		wooting.setArrayKey(3, 7, constants.colors.white)
		wooting.setArrayKey(4, 8, constants.colors.white)

		wooting.flushKeys()
	}

	/**
	 * Initializes the instance attributes
	 */
	constructor() {
		this.games = [{
			name: 'Tromino',
			demo: {
				preview: [
					[0, 0, 0, 0, 0, 0, 5, 4, 4, 4],
					[0, 1, 1, 0, 0, 0, 5, 5, 0, 0],
					[0, 0, 1, 0, 0, 0, 0, 0, 2, 2],
					[0, 0, 0, 0, 0, 0, 3, 3, 3, 2]
				],
				get colors() {
					const colors = utils.generateNiceColors()

					;(arr => {
						for(let i = arr.length - 1; i > 0; i--) {
							const j = Math.floor(Math.random() * (i + 1))
							;[arr[i], arr[j]] = [arr[j], arr[i]]
						}
						return arr
					})(colors)

					colors.splice(0, 0, constants.colors.clear)
					return colors
				}
			}
		}, {
			name: 'Dino',
			demo: {
				preview: [
					[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
					[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
					[0, 1, 0, 0, 2, 0, 0, 0, 0, 2],
					[0, 1, 0, 0, 2, 0, 0, 2, 0, 2]
				],
				get colors() {
					const dinoColors = [
						[255, 0, 0],
						[0, 0, 255],
						[255, 255, 0],
						[0, 255, 255],
						[255, 0, 255]
					]

					return [
						constants.colors.clear,
						dinoColors[Math.floor(Math.random() * dinoColors.length)],
						constants.colors.green,
					]
				}
			}
		}]
		this.selectedIndex = .5

		this.timers = {
			controlsIndicators: 0,
			selectionIndicators: 0
		}

		this.fork = {}
	}

	/**
	 * Starts the menu logic
	 */
	start() {
		Menu.clearKeyboard()
		Menu.drawGreeting()

		this.startIoHook()
		this.startControlsIndicators()
	}

	/**
	 * @return {number} The current selection
	 */
	get selection() {
		return this.selectedIndex
	}

	/**
	 * Sets the current `selectionIndex` to given `value` and limits it to the possible values aswell
	 * @param value The index of the game to select
	 */
	set selection(value) {
		const previous = this.selectedIndex
		this.selectedIndex = Math.floor(Math.max(0, Math.min(value, this.games.length - 1)))

		if(this.selectedIndex !== previous) {
			this.drawDemo(this.selectedIndex)
		}

		if(!this.timers.selectionIndicators) this.startSelectionIndicators()
	}

	/**
	 * Starts an interval that signals which controls are available until one key was pressed
	 */
	startControlsIndicators() {
		wooting.setArrayKey(0, 0, constants.colors.red) // ESC

		this.timers.controlsIndicators = setInterval(() => {
			const percent = this.hitAKeyYet ? 1 : ((Math.sin(new Date() / 125) / 2) + .5)
				, orange = constants.colors.orange.map(value => value * percent)

			wooting.setArrayKey(5, 14, orange) // Arrow left
			wooting.setArrayKey(5, 16, orange) // Arrow right

			wooting.flushKeys()

			if(this.hitAKeyYet) { // stop the flashing
				clearInterval(this.timers.controlsIndicators)
			}
		}, 20)
	}

	/**
	 * Starts an interval that signals what game currently is selected. Flashes `Return` and relevant F-keys
	 */
	startSelectionIndicators() {
		this.timers.selectionIndicators = setInterval(() => {
			const percent = (Math.sin(new Date() / 125) / 2) + .5
				, orange = constants.colors.orange.map(value => value * (1 - percent))
				, white = constants.colors.white.map(value => value * percent)

			wooting.setArrayKey(3, 13, orange) // Return

			Array(8).fill(0).forEach((_, index) => { // F-keys
				wooting.setArrayKey(0, 2 + index, (this.selectedIndex === parseInt(index / 4)) ? white : constants.colors.gray)
			})

			wooting.flushKeys()
		}, 20)
	}

	/**
	 * Draws a demo of the game to the main keyboard area
	 * @param gameIndex The index of the game whose demo shall be drawn
	 */
	drawDemo(gameIndex) {
		wooting.setArrayKey(2, 0, constants.colors.white) // tab, maybe that will make it clear that the game is moving to the right?

		const demo = this.games[gameIndex].demo
		    , colors = demo.colors

		demo.preview.forEach((row, rowIndex) => {
			row.forEach((colorId, colIndex) => {
				wooting.setArrayKey(rowIndex + 1, colIndex + 1 + Number(rowIndex === 3), colors[colorId])
			})
		})
		wooting.flushKeys()
	}

	/**
	 * Kills off all the timers and passes the selected game's name on to the bootstrapper
	 */
	startGame() {
		ioHook.stop()
		ioHook.removeAllListeners()

		Object.values(this.timers).forEach(timer => clearInterval(timer))

		this.fork.resolve(this.games[this.selectedIndex].name)
	}

	/**
	 * Starts polling key events with the `iohook` npm package
	 */
	startIoHook() {
		ioHook.start(false)
		ioHook.on('keydown', event => {
			switch(event.rawcode) {
				case keyCodes.escape: {
					for(let col = 0; col < wooting.keyboardDimensions.columns; col++) {
						for(let row = 0; row < wooting.keyboardDimensions.rows; row++) {
							wooting.resetKey(row, col)
						}
					}

					process.exit(exitCodes.user)
					break
				}

				case keyCodes.left: {
					this.selection--
					break
				}
				case keyCodes.right: {
					this.selection++
					break
				}

				case keyCodes.enter: {
					if(this.selection === parseInt(this.selection)) this.startGame()
					break
				}

				default: return
			}
			this.hitAKeyYet = true
		})
	}
}
