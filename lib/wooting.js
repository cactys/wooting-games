// core packages:

const os = require('os')
    , path = require('path')


// npm packages:

const ffi = require('ffi-napi')
    , Struct = require('ref-struct')
    , { types, refType } = require('ref')


types.WOOTING_DEVICE_TYPE = types.short
types.WOOTING_USB_META = Struct({
	connected: types.bool,
	model: types.CString,
	max_rows: types.uint8,

	max_columns: types.uint8,
	led_index_max: types.uint8,
	device_type: types.WOOTING_DEVICE_TYPE
})


/**
 * @typedef WootingRgbSdkWrapper
 * @returns {WootingRgbSdkWrapper} WootingRgbSdkWrapper
 */
module.exports = (() => {
	const methodMapping = {
		'wooting_rgb_kbd_connected': [ types.bool, [] ],
		'wooting_rgb_device_info': [ refType(types.WOOTING_USB_META), [] ],

		'wooting_rgb_close': [ types.bool, [] ],
		'wooting_rgb_reset_rgb': [ types.bool, [] ],
		'wooting_rgb_direct_set_key': [ types.bool, [types.uint8, types.uint8, types.uint8, types.uint8, types.uint8] ],
		'wooting_rgb_direct_reset_key': [ types.bool, [types.uint8, types.uint8] ],
		'wooting_rgb_array_set_single': [ types.bool, [types.uint8, types.uint8, types.uint8, types.uint8, types.uint8] ],
		'wooting_rgb_array_update_keyboard': [ types.bool, [] ]
	}

	let nativesPath
	switch(os.type()) {
		case 'Linux': {
			nativesPath = path.join(__dirname, 'natives', `libwooting-rgb-sdk`)
			break
		}
		case 'Windows_NT': {
			nativesPath = path.join(__dirname, 'natives', `wooting-rgb-sdk${os.arch().replace('x', '')}`)
			break
		}
		default: {
			console.error(`Unsupported OS: ${os.type()}`)
			return process.exit(1)
		}
	}

	let lib
	try {
		lib = ffi.Library(nativesPath, methodMapping)
	} catch(err) {
		if(os.type() === 'Windows_NT' && err.message.includes('Win32 error 193')) {
			try {
				lib = ffi.Library(nativesPath.replace('64', ''), methodMapping)
			} catch(err) {
				console.warn('RGB SDK not found or not compatible:')
				throw err
			}
		} else if(os.type() === 'Linux' && err.message.includes('.so')) {
			const dependencies = [
				'libusb-1.0', 'libusb-dev', 'hidapi', 'libhidapi-dev'
			]
			console.error(`Please make sure to install these packages:\n- ${dependencies.join('\n- ')}`)
			process.exit(1)
		} else throw err
	}

	const deviceInfo = lib.wooting_rgb_device_info().deref()

	/**
	 * Function wrappers for the Wooting RGK SDK
	 * @typedef {{...any}} WootingRgbSdkWrapper
	 */
	return {
		isConnected: lib.wooting_rgb_kbd_connected,
		isWootingTwo: deviceInfo.model === 'Wooting two',
		keyboardDimensions: {
			columns: deviceInfo.max_columns,
			rows: deviceInfo.max_rows,
		},

		close: lib.wooting_rgb_close,
		reset: lib.wooting_rgb_reset_rgb,
		flushKeys: lib.wooting_rgb_array_update_keyboard,

		/**
		 * Sets a single key in the given `row` and `col`umn immediately
		 * @param row The row of the key
		 * @param col The column of the key
		 * @param {RgbColor} color The color for the key
		 * @return {boolean}
		 */
		setKey: (row, col, color) => { // Updating the array along with painting the new key color
			lib.wooting_rgb_array_set_single(row, col, ...color)
			return lib.wooting_rgb_direct_set_key(row, col, ...color)
		},

		/**
		 * Resets a single key in the given `row` and `col`umn immediately
		 * @param row The row of the key
		 * @param col The column of the key
		 * @return {boolean}
		 */
		resetKey: (row, col) => {
			return lib.wooting_rgb_direct_reset_key(row, col)
		},

		/**
		 * Sets a key in the given `row` and `col`umn
		 * @param row The row of the key
		 * @param col The column of the key
		 * @param {RgbColor} color The color for the key
		 * @return {boolean}
		 */
		setArrayKey: (row, col, color) => {
			return lib.wooting_rgb_array_set_single(row, col, ...color)
		}
	}
})()
