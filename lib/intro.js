/**
 * This is a short intro script that just renders "3, 2, 1, GO" to the center of the keyboard and then resolves a promise
 */
/**/

const wooting = require('./wooting')


const signs = [{
	keys: [
		[0, 0, 0, 0, 0, 1, 1, 1, 1],
		[0, 0, 0, 0, 0, 0, 0, 0, 1],
		[0, 0, 0, 0, 0, 0, 1, 1, 1],
		[0, 0, 0, 0, 0, 0, 0, 0, 1],
		[0, 0, 0, 0, 0, 1, 1, 1, 1],
	],
	color: [255, 255, 255]
}, {
	keys: [
		[0, 0, 0, 0, 0, 1, 1, 1, 1],
		[0, 0, 0, 0, 0, 0, 0, 0, 1],
		[0, 0, 0, 0, 0, 0, 0, 1, 0],
		[0, 0, 0, 0, 0, 0, 1, 0, 0],
		[0, 0, 0, 0, 0, 1, 1, 1, 1],
	], color: [255, 255, 255]
}, {
	keys: [
		[0, 0, 0, 0, 0, 0, 1, 1, 0],
		[0, 0, 0, 0, 0, 0, 0, 1, 0],
		[0, 0, 0, 0, 0, 0, 0, 1, 0],
		[0, 0, 0, 0, 0, 0, 0, 1, 0],
		[0, 0, 0, 0, 0, 0, 0, 1, 0],
	], color: [255, 255, 255]
}, {
	keys: [
		[1, 1, 1, 1, 0, 1, 1, 1, 1],
		[1, 0, 0, 0, 0, 1, 0, 0, 1],
		[1, 0, 1, 1, 0, 1, 0, 0, 1],
		[1, 0, 0, 1, 0, 1, 0, 0, 1],
		[1, 1, 1, 1, 0, 1, 1, 1, 1],
	], color: [0, 255, 0]
}]

const renderSign = index => {
	const sign = signs[index]
	if(!sign) return false

	sign.keys.forEach((row, rowIndex) => {
		row.forEach((col, colIndex) => {
			let plus = 1
			if(rowIndex === 4) plus++
			if(rowIndex !== 0) plus++
			else if(colIndex < 5) plus++

			const opacity = row[colIndex]
			wooting.setArrayKey(rowIndex, colIndex + plus, sign.color.map(value => value * opacity))
		})
	})
	wooting.flushKeys()

	return true
}

module.exports = async() => {
	return new Promise((resolve => {
		for(let row = 0; row < wooting.keyboardDimensions.rows; row++) {
			for(let col = 0; col < wooting.keyboardDimensions.columns; col++) {
				wooting.setArrayKey(row, col, [0, 0, 0])
			}
		}
		wooting.flushKeys()

		const next = index => {
			if(renderSign(index)) {
				setTimeout(() => next(index + 1), 1000)
			} else {
				resolve()
			}
		}
		next(0)
	}))
}
