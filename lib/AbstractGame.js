/**
 * okay
 */
module.exports = class AbstractGame {
	/**
	 * The constructor of the game
	 */
	constructor() {
		this.fork = {
			resolve: () => {},
			reject: () => {}
		}
	}

	/**
	 * Initiates the game, clears colors and highlights control buttons
	 * @return {Promise<void>}
	 */
	async start() {
		return new Promise(((resolve, reject) => {
			this.fork = { resolve, reject }
		}))
	}

	/**
	 * Performs a game logic step
	 */
	tick() {}
}
