/**
 * Some utils
 */
/**/

module.exports = {
	/**
	 * Spits out nice colors
	 * @return {RgbColor[]}
	 */
	generateNiceColors: () => Array(12).fill(0).map((_, i) => Array(3).fill(8).map((mult, pos, arr) => Math.max(1, Math.min(Math.floor(((Math.cos(Math.PI * ((((pos * mult) + i) % (mult * arr.length)) / 6)) / 2) + .5) * 3) * 128, 256)) - 1))
}
