# Metaa's *Wooting* games [![NPM package version](https://img.shields.io/npm/v/@metaa/wooting-games.svg)](https://www.npmjs.com/package/@metaa/wooting-games)
This package contains two simple games that can be played on [Wooting](https://wooting.io/) keyboards by using the
[Wooting RGB SDK](https://github.com/WootingKb/wooting-rgb-sdk) to render everything on the keyboard itself!

## Installation

```bash
npm install -g @metaa/wooting-games
```

## Usage

```bash
wooting-games
```

### Main menu
Once you get to the game menu, the keyboard will indicate which keys can be pressed to do things.

To select a game, use either the `left arrow key` or the `right arrow key`. The F-keys on top and a demo of the selected
game will indicate what your selection is.

You can start the game by hitting `Return`.

##### Controls

Key                     | Action
---                     | ---
`ESC`                   | Quit program
`Return`                | Start selected game
Arrow `left`            | Select Wootromino
Arrow `right`           | Select CactiJump

### Games included

#### Wootromino
> This game was inspired by [Tetris](https://en.wikipedia.org/wiki/Tetris).

---

You control colorful Russian [Tromino](https://en.wikipedia.org/wiki/Tromino) bricks that slowly fall to the ground.

> Please note that **down** is on the **right** of your keyboard and that **up** is on the **left** of your keyboard.

Move and rotate the *Tromimo* while it's falling to line it up with other Trominoes and fill the empty space on your
keyboard (not the space bar though).

Once there is a full vertical line of Tromino, this line will clear and the blocks above will fall down accordingly. Try
to survive as long as you can as the game gets faster with every 4 lines cleared.

##### Controls

Key                     | Action
---                     | ---
`ESC`                   | Quit game
`Return`                | Pause game
`F1` to `F12`           | Change speed
`NumLock`               | Toggle preview (only on `Wooting 2`)
Arrow `up`              | Move tromino to the right
Arrow `right`           | Move tromino down
Arrow `down`            | Move tromino left
Arrow `left`            | Drop tromino to the ground
`CTRL right` / `Space`  | Rotate Tromino

#### 🌵 CactiJump
> This game was inspired by [Google Chrome](https://www.google.com/chrome/)'s T-Rex game that you get to play when you
are offline (or when you explicitly choose to play it by visiting `chrome://dino`).

---

You control a small colorful dinosaur (but it's not a [Yoshi](https://en.wikipedia.org/wiki/Yoshi) (is it a `Woot`shi?
🤔)) that decided to run into the desert. You have to help it jump over the evil 🌵 cacti that block the way, otherwise
it will die a horrible and painful death.

The more 🌵 cacti the dinosaur jumps over, the faster it will run.

##### Controls

Key                     | Action
---                     | ---
`ESC`                   | Quit game
`Return`                | Pause game
`F1` to `F6`            | Change speed
`Space` / `Arrow up`    | Jump

## More information
Visit [dev.wooting.io](https://dev.wooting.io/) or [wooting.io](https://wooting.io/) to learn more about what you can do
with Wooting keyboards.
