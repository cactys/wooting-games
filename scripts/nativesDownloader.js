/**
 * This script downloads the native files of the RGB SDK and puts them into their place
 */
/**/

// core packages:

const fs = require('fs').promises
    , os = require('os')
    , path = require('path')


// npm packages:

const downloadRelease = require('download-github-release')


const pkg = require('../package.json')
const nativesPath = path.join(__dirname, '..', 'lib', 'natives')


const cleanNatives = (additionalFilter = () => true) => {
	return fs.readdir(nativesPath)
		.then(files => {
			files
				.filter(file => !file.startsWith('.') && additionalFilter(file))
				.forEach(file => fs.unlink(path.join(nativesPath, file)))
		})
}


const osFilters = (type => ({
	Linux: {
		releases: asset => !!asset.name.match(/^wooting-rgb-sdk-v(\d+(\.\d+)*)+-linux-x\d+\.zip$/),
		natives: file => !file.endsWith('.so')
	},
	Windows_NT: {
		releases: asset => !!asset.name.match(new RegExp(`^wooting-rgb-sdk-v(\\d+(\\.\\d+)*)+-win-${os.arch().replace('32', '86')}\\.zip$`)),
		natives: file => !file.endsWith('.dll')
	}
})[type])(os.type())

if(!osFilters) {
	console.error(`Apparently there are no native files for your system yet (or they are pretty new and not supported by this package yet).\nIf you feel like this is wrong, please create an issue at ${pkg.bugs.url}.`)
	process.exit(1)
}


cleanNatives()
	.then(() =>
		downloadRelease(
			'WootingKb', 'wooting-rgb-sdk',
			nativesPath,
			release => release.prerelease === false,
			osFilters.releases,
			false
		)
	).then(
		() => cleanNatives(osFilters.natives),
		console.error
	)
