#!/usr/bin/env node


if(require.main !== module) {
	console.error('Please do not require this package. There is no point in doing so.')
	return
}

/**
 * This is our main bootstrapper. It takes care of initializing a menu, showing
 * a short intro scene and then booting up the selected game.
 */

const iohook = require('iohook')

const wooting = require('./lib/wooting')
    , menu = require('./lib/menu')
    , intro = require('./lib/intro')
    , exitCodes = require('./lib/exitCodes')

if(!wooting.isConnected()) {
	console.error('Could not detect a Wooting keyboard. Did you plug it in?')
	process.exit(exitCodes.noKeyboard)
}


// Make sure the lights go back to normal after process exits
process.on('SIGINT', () => {
	process.exit(0)
})
process.on('exit', () => {
	iohook.unload()
	wooting.close()
})

const run = () => {
	menu.open()
		.then(chosenGame => {
			const gameName = `${chosenGame}Game`

			let gameClass
			try {
				gameClass = require(`./lib/games/${gameName}`)
			} catch(err) {
				console.error(`Game "${gameName}" not found.`)
				console.error(err)

				process.exit(exitCodes.other)
			}

			return intro()
				.then(() => {
					try {
						new gameClass().start()
							.then(run)

					} catch(err) {
						console.warn('Oops!')
						console.error(err)

						process.exit(exitCodes.other)
					}
				})
		})
}

run()
